/*Utilisation de la méthode useState qui va permettre de 
créer les données d'un composant*/
import { useState } from "react";
import "./index.css"
function App() {

  const data = [
    "Mammifères",
    "Insectes",
    "Poissons",
    "Reptiles",
    "Batraciens",
    "Mollusques",
    "Cnidaires"
  ];
  // Déclaration de la valeur de recherche initiale
  const [searchValue, setSearchValue] = useState('');
  // Déclaration des données filtrées
  const [filteredData, setFilteredData] = useState(data);

  // Fonction de gestion de la recherche
  const handleSearch = e => {
    // Mise à jour de la valeur de recherche
    setSearchValue(e.target.value);
    // Filtrage des données en fonction de la valeur de recherche
    const filteredData = data.filter(item =>
      item.toLowerCase().includes(searchValue.toLowerCase())
    );
    // Mise à jour des données filtrées
    setFilteredData(filteredData);
  };

  return (
    <div>
      <h1>Exercice 1: Créer un filtre de recherche avec React</h1>
      {/* Champ de saisie de recherche */}
      <input
        type="text"
        value={searchValue}
        onChange={handleSearch}
        placeholder="Rechercher..."
        className="search-input"
      />
      {/* Liste des éléments filtrés */}
      <ul className="list">
        {filteredData.map(item => (
          <li key={item} className="list-item">
            {item}
          </li>
        ))}
      </ul>
    </div>
  );
}
// Exportation du composant App
export default App;

